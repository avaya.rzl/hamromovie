<?php

/*
  |--------------------------------------------------------------
  | Programmed By Raghu Chaudhary 2014-Apr-19
  |--------------------------------------------------------------
 */

class CaptchaMath
{

    private static $num1;
    private static $num2;
    private static $secretKey;
    private static $captcha;
    protected static $instance = NULL;

    function __construct()
    {
        self::$num1 = rand(1, 10);
        self::$num2 = rand(1, 9);
        self::$secretKey = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        self::$captcha = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);
    }

    public static function DisplayNum()
    {
        return self::$num1 . " + " . self::$num2;
    }

    public static function HiddenSum()
    {
        return $_SESSION[self::$captcha][self::$secretKey] = self::$num1 + self::$num2;
    }

    public static function Instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = New captchaMath();
        }
        return self::$instance;
    }

    function __destruct()
    {
        unset($_SESSION[self::$captcha]);
        //session_destroy();
    }

}
