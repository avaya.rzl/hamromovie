<?php
class HitCounter {
    private $fileName; //the text file that contains the current number of hits
        
        function __construct($file) {        
            $this->fileName = $file;    //$file is the text file that contains the number of hits    
                if (!file_exists($file)) {  
                    file_put_contents($file,"0"); 
                } 
        }    
        
    //getCounter() - reads the contents of the text file specified in $fileName and returns the value. 
    //The contents of the file will be the current number of hits. The function returns the value in the file.
        public function getCounter() {
            $hits = file_get_contents($this->fileName);
            return $hits;
        }
    
    //setCounter($counter) - writes the value specified by $counter to the text file
    //I changed the name of the parameter $counter to $addHits, since that's what I'm using setCounter for                
        public function setCounter($addHits) {
            $this->addHits = file_put_contents($this->fileName, $addHits);
        }
        
    //updateCounter() - gets the counter from the text file, adds one to it, 
    //saves the new count back to the text file, and returns the new value to the caller
        public function updateCounter() {
            $newValue = $this->getCounter() +1; 
                $this->setCounter($newValue);
            $totalHits = $this->getCounter();
            return $totalHits;
        }
}
?>