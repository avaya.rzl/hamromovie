<?php

namespace Site\Model;

use DB;

class User
{

    private static $_tableName = "ramro_movie_user";

    public static function CheckEmail($email)
    {

        if (DB::dlookup('email', self::$_tableName, 'email=?', [$email])) {
            return true;
        } else {
            return false;
        }

    }

    public static function CheckUser($email, $pass)
    {

        $id = DB::dlookup('id', self::$_tableName, "email=? AND pass=? AND social_media=? AND role=3", [$email, $pass,'normal']);

        if ($id) {

            return $id;

        } else {

            return false;
        }

    }

    public static function SaveUser($rawData)
    {
        if (DB::insert(self::$_tableName, $rawData)) {

            return DB::insert_id();
        } else {

            return false;
        }

    }

    public static function GetUserById($user_id)
    {
        DB::select('*', self::$_tableName, 'id=?', [$user_id]);
        $result = DB::fetch_assoc();

        if ($result) {

            return $result;

        } else {

            return false;
        }
    }


    public static function GetUserBySocialMedia($social_id, $social_media)
    {

        DB::select('*', self::$_tableName, 'social_media=? AND social_id=?', [$social_media, $social_id]);
        $row = DB::fetch_assoc();
        if ($row) {
            return $row;
        } else {
            return false;
        }

    }

    public static function updateUser($data, $user_id){
        if( DB::update(self::$_tableName, $data, "id=?", [$user_id]) ){
            return true;
        }else{
            return false;

        }
    }

}