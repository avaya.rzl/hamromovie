<?php
App::hook('slim.before.dispatch', function () {

/*===========================================================================
 * STORE SESSION DATA BEFORE LOAD
 *
 *===========================================================================
 *
 */
    // SESSION DATA
    $data = array();
    $session = New Session();
    $data['logged']= $session->getSession('SiteloggedIn');
    $data['base_url'] = BASE_URL;

    if ($data['logged'] == true) {
        
        $data['userData'] = [

            'id' => $session->getSession('id'),
            'socialId' => $session->getSession('socialId'),
            'fullname' => $session->getSession('fullname'),
            'fname' => $session->getSession('fname'),
            'lname' => $session->getSession('lname'),
            'email' => $session->getSession('email'),
            'profilePic' => $session->getSession('profilePic'),
            'socialMedia' => $session->getSession('socialMedia')

        ];
    }

    $data['search_query'] = isset($_GET['q'])? $_GET['q'] : '';

/*===========================================================================
 * CATEGORY DATA FOR MENU
 *
 *===========================================================================
 *
 */
    DB::query('SELECT id,title FROM ramro_movie_category ORDER BY title ASC');
    $data['categories'] = DB::fetch_assoc_all();


    
/*===========================================================================
 * FOOTER DATA BEFORE LOAD
 *
 *===========================================================================
 *
 */

    DB::query('SELECT id,title,url FROM ramro_movie WHERE status = 1 ORDER BY id DESC LIMIT 4');
    $data['footerLatest'] = DB::fetch_assoc_all();

    DB::query('SELECT id,title,url FROM ramro_movie WHERE status = 1 ORDER BY viewed DESC LIMIT 4');
    $data['footerTopViewed'] = DB::fetch_assoc_all();

    App::view()->appendData($data);

});

App::hook('slim.after.dispatch', function () {

    $data = array();
    App::view()->appendData($data);
});