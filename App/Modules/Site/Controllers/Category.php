<?php
/**
 * Created by PhpStorm.
 * User: Bidhee
 * Date: 4/9/2015
 * Time: 11:54 AM
 */

namespace Site\Controller;

use App;
use DB;
use Input;
use Request;
use Response;
use View;
use Pagination;


class Category {

    public function Show($cat_name){
        try {

            $limit = 12;
            $start = 0;

            $p = Input::get('p');
            isset($p) ? $page = preg_replace("/[^0-9]/", " ", trim($p)) : $page = 1;

            $start = ($page - 1) * $limit;

            $sql = "SELECT

                        m.id as mid,
                        m.title as title,
                        m.image as image,
                        m.details as details,
                        m.trailer_link  as trailerLink,
                        m.full_link  as fullLink,
                        m.price  as price,
                        m.movie_type  as movieType,
                        m.movie_tag  as movieTag,
                        m.cast  as cast,
                        m.director  as director,
                        m.music  as music,
                        m.language  as language,
                        m.subtitle  as subtitle,
                        m.viewed  as view,
                        m.url  as url

                    FROM
                        ramro_movie as m

                    RIGHT JOIN

                        ramro_movie_category as c

                    ON
                        m.cat_id = c.id

                    WHERE
                        m.status=1
                    AND
                        c.title= ?

                       ";

            // COUNT RECORDS
            DB::query($sql,array($cat_name), true);
            $data['totalRecords'] = $count = count(DB::fetch_assoc_all());

            // DEFAULT ASSIGN ORDER
            $sql .= " ORDER BY m.id DESC LIMIT $start, $limit";

            // FINAL QUERY FOR THE RESULT
            DB::query($sql,array($cat_name), true);
            $data['results'] = DB::fetch_assoc_all();

            if (!$data['results']) {
                Response::redirect(App::urlFor('home'));
            }
            // PAGINATION RESULT AND LINK
            $pg = new Pagination();
            $data['pagination'] = $pg->create_links($limit, $page, App::urlfor('category-site') .'/'.$cat_name. '?p=', $count);

        } catch (ResourceNotFoundException $e) {

            $data['errMsg'] = $e->getMessage();
        }

        // ASSIGN PAGE TO FORM TO RESUBMIT
        $data['page'] = $page;
        $data['categorytitle'] = DB::dlookup('title','ramro_movie_category','title=?',array($cat_name));

        View::display('@Site/category/list-all.twig', $data);
    }

}