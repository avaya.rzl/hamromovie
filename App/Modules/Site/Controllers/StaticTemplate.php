<?php

namespace Site\Controller;

use App;
use DB;
use Input;
use Request;
use Response;
use Session;
use Validate;
use View;
use Site\Model\User as UserModel;



class StaticTemplate
{

    public function PrivacyPolicy()
    {
        $data = [];
        View::display('@Site/static/privacy-policy.twig', $data);

    }

    public function TermsAndCondition()
    {
        $data = [];
        View::display('@Site/static/terms-and-condition.twig', $data);

    }

    public function ContactUs()
    {
        $data = [];

        if (sizeof(Input::post()) > 0) {

            $input = (object)Input::post();


            $data['v'] = Validate::Instance(); //OBJECT VALIDATE
            Validate::Str($input->name, $displayName = 'Name', $req = TRUE);
            Validate::Email($input->email, $displayName = 'Email', $req = TRUE);
            Validate::Str($input->message, $displayName = 'Message', $req = TRUE);

            if (Validate::IsFine()) {

                try{
                    $mail = new \PHPMailer();

                    $mail->isSMTP();                                      // Set mailer to use SMTP
                    $mail->Host = MAILER_HOST;  // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                               // Enable SMTP authentication
                    $mail->Username = MAILER_USERNAME;                 // SMTP username
                    $mail->Password = MAILER_PASSWORD;                           // SMTP password
                    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = MAILER_PORT;                                    // TCP port to connect to

//                    $mail->SMTPDebug  = 1;

                    $mail->From = $input->email;
                    $mail->FromName = $input->name;
                    $mail->addAddress(FEEDBACK_RECEIVER_EMAIL, FEEDBACK_RECEIVER_NAME);     // Add a recipient
//                $mail->addAddress('ellen@example.com');               // Name is optional
//                $mail->addReplyTo('info@example.com', 'Information');
//                $mail->addCC('cc@example.com');
//                $mail->addBCC('bcc@example.com');

//                $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//                $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
                    $mail->isHTML(true);                                  // Set email format to HTML

                    $messageBody = '<p><strong>Name</strong> &nbsp; &nbsp;'.$input->name.'</p>';
                    $messageBody .= '<p><strong>Email</strong> &nbsp; &nbsp;'.$input->email.'</p>';
                    $messageBody .= '<p><strong>Message</strong> &nbsp; &nbsp;'.$input->message.'</p>';

                    $mail->Subject = 'Feedback And Views';
                    $mail->Body    = $messageBody;
                    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                    if(!$mail->send()) {
//                    print_r($mail->ErrorInfo);
//                    print_r($mail->SMTPDebug);
//                    die;
                        $data['errMsg'] = 'Message Could not be send. Please retry after a while.';  // .$mail->ErrorInfo;
                    } else {
                        $data['successMsg'] = 'Mail sent successfully.';
                    }
                }catch(\phpmailerException $e){
                    $data['errMsg'] = $e->getMessage();
                }


            }else{
                $data['errMsg'] = Validate::ErrorList();
            }
        }


        View::display('@Site/static/contact.twig', $data);

    }

    public function AboutUs()
    {
        $data = [];
        View::display('@Site/static/about-us.twig', $data);

    }



}