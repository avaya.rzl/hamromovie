<?php

namespace Site\Controller{

use App;
use DB;
use Input;
use Request;
use Response;
use View;
use Session;
use PayPal\Api\Amount;
use PayPal\Api\Agreement;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Plan;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Currency;
use PayPal\Api\ChargeModel;
use PayPal\Api\BillingAgreement;
use PayPal\Api\BillingPlan;
use PayPal\Api\WebProfiles;
use PayPal\Api\PatchRequest;
use PayPal\Api\Patch;
use PayPal\Common\PayPalModel;
use Site\Model\PaymentClass as ModelPaymentClass;
use Site\Model\Video as MovieModel;

class PaymentClass {

//    private static $clientId 	= "AQo7lqgl7sJAvUPJwisBRCJVbYHzAaQFV2kl0h-ckOoM6216WYnl8Y_oQBhioPgDwQCF4NhF8a-QRwa5";
//    private static $secretId 	= "ELdNugNfTxOQHRQVqMXrTtO__INddF6PAp9owKJcRBnmhJSS19GF72ePrXrss8pcclLlurLnK9aBbpN5";

    private static $clientId 	= PAYPAL_CLIENT_ID;
    private static $secretId 	= PAYPAL_SECRETE_ID;

//        private static $clientId 	= "EPsqZ3ODJT85IdMnd9HV4rLIBgS3EBD7s1eoeBJ_fNL-vdd3tjTMWDORm-ZI05MlwIkUpx2lureNh2uA";
//        private static $secretId 	= "AR_vuYfdlP_D3iFhF8SdLGFk2zAElefj-F9WPmPmQjUZjiApsr587enpSjC8Mae0wXNjThktG8MCCIyk";

//        private static $clientId 	= "bhattabhakta-facilitator_api1.gmail.com";
//        private static $secretId 	= "YCHSL2K2CHJKDVS4";

//    private static $paymentMode = "sandbox";
    private static $paymentMode = PAYPAL_PAYMENT_MODE;
    private static $host	= BASE_URL;
   
    
    public static function ConnectApi(){
    
    	$apiContext = '';
    	$apiContext = new ApiContext(new OAuthTokenCredential(self::$clientId, self::$secretId));
	    $apiContext->setConfig(
	        array(
	            'mode' => self::$paymentMode,
	            'log.LogEnabled' => true,
	            'log.FileName' => 'App/Storage/paypal/PayPal.log',
	            'log.LogLevel' => 'FINE'
	        )
	);
	
     return $apiContext;
    	
    }
    
    public function Premium($movie_id = 0) {

        $session = New Session();
        $logged = $session->getSession('SiteloggedIn');
//        if ($logged == true) {
//            Response::redirect(App::urlFor('home'));
//        }

        $apiContext = new ApiContext(new OAuthTokenCredential(self::$clientId, self::$secretId));
        $apiContext->setConfig(
                array(
                    'mode' => self::$paymentMode,
                    'log.LogEnabled' => true,
                    'log.FileName' => 'App/Storage/paypal/PayPal.log',
                    'log.LogLevel' => 'FINE'
                )
        );

        $returnUrl = self::$host.'/approval-premium?success=true';
    	$cancelUrl = self::$host.'/approval-premium?success=false';

        // # Create Payment using PayPal as payment method
        // This sample code demonstrates how you can process a
        // PayPal Account based Payment.
        // API used: /v1/payments/payment
        // ### Payer
        // A resource representing a Payer that funds a payment
        // For paypal account payments, set payment method
        // to 'paypal'.
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");
//print_r($movie_id); exit;
        // Get Info From Movie database
        $movie = ModelPaymentClass::GetProductInfo($movie_id);
//        print_r($movie->id); exit;
        // ### Itemized information
        // (Optional) Lets you specify item wise
        // information
        $item1 = new Item();
        $item1->setName($movie->title)
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($movie->price);
//        $item2 = new Item();
//        $item2->setName('Granola bars')
//                ->setCurrency('USD')
//                ->setQuantity(5)
//                ->setPrice(2);

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

        // ### Additional payment details
        // Use this optional field to set additional
        // payment information such as tax, shipping
        // charges etc.
//        $details = new Details();
//        $details->setShipping(1.2)
//                ->setTax(1.3)
//                ->setSubtotal(17.50);
        // ### Amount
        // Lets you specify a payment amount.
        // You can also specify additional details
        // such as shipping, tax.
        $amount = new Amount();
        $amount->setCurrency("USD")
                ->setTotal($movie->price);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it.
        $transaction = new Transaction();
        $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription("Payment description")
                ->setInvoiceNumber(uniqid());

        // ### Redirect urls
        // Set the urls that the buyer must be redirected to after
        // payment approval/ cancellation.
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($returnUrl)
                ->setCancelUrl($cancelUrl);

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent set to 'sale'
        $payment = new Payment();
        $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));

        // For Sample Purposes Only.
        //$request = clone $payment;
        // ### Create Payment
        // Create a payment by calling the 'create' method
        // passing it a valid apiContext.
        // (See bootstrap.php for more on `ApiContext`)
        // The return object contains the state and the
        // url to which the buyer must be redirected to
        // for payment approval
        try {

            $payment->create($apiContext);
            $payment_id = $payment->getId();
            $hashPaymentId = md5($payment_id);
            $session = New Session();
            $user_id = $session->getSession('id');
            $session->setSession(['payment_id' => $hashPaymentId]);

            $userDataPayemnt = [

                'user_id' => $user_id,
                'invoice' => $transaction->getInvoiceNumber(),
                'movie_id' => $movie->id,
                'payment_for' => 'premium',
                'payment_id' => $payment_id,
                'hash_payment_id' => $hashPaymentId,
                'created_at' => CUR_TIME
            ];

            ModelPaymentClass::SavePayerInfo($userDataPayemnt);
        } catch (Exception $ex) {
            print_r($ex->getMessage());
            exit();
        }

        // ### Get redirect url
        // The API response provides the url that you must redirect
        // the buyer to. Retrieve the url from the $payment->getApprovalLink()
        // method
        $approvalUrl = $payment->getApprovalLink();
//        print_r($approvalUrl); exit;
        Response::redirect($approvalUrl);

        View::display('@Site/payment/payment.twig', $payment);
    }// Premium

    public function ApprovalPremium() {

        $apiContext = new ApiContext(new OAuthTokenCredential(self::$clientId, self::$secretId));
	$apiContext->setConfig(
	        array(
	            'mode' => self::$paymentMode,
	            'log.LogEnabled' => true,
	            'log.FileName' => 'App/Storage/paypal/PayPal.log',
	            'log.LogLevel' => 'FINE'
	        )
	);
        
        if (isset($_GET['success']) && $_GET['success'] == 'true') {

            $paymentId = $_GET['paymentId'];
            $payment = Payment::get($paymentId, $apiContext);

            $execution = new PaymentExecution();
            $execution->setPayerId($_GET['PayerID']);

            try {
                $result = $payment->execute($execution, $apiContext);

                //ResultPrinter::printResult("Executed Payment", "Payment", $payment->getId(), $execution, $result);

                try {

                    $payment = Payment::get($paymentId, $apiContext);

                    if (ModelPaymentClass::UpdatePayerInfo($payment->getId())) {

                        $movieData = ModelPaymentClass::GetPaypalTransactionData($payment->getId());

                        switch ($movieData->payment_for) {

                            case 'premium':

                                $dataPremium = [
                                    'movie_id' => $movieData->movie_id,
                                    'status' => 1,
                                    'user_id' => $movieData->user_id,
                                    'expires_at' => date("Y-m-d H:i:s", strtotime('+24 hours')),
                                    'created_at' => CUR_TIME,
                                    'updated_at' => CUR_TIME
                                ];

                                ModelPaymentClass::SavePremiumDataToUser($dataPremium);
                                break;

                            case 'subscription':

                                break;
                        }

                        $movie = MovieModel::GetVideoByIdOnly($movieData->movie_id);

//                        Response::redirect(App::urlFor('video-detail', ['url'=>$movie['url'], 'video_id'=>$movieData->movie_id]).'?ps=s');
                        Response::redirect(App::urlFor('video-detail', ['url'=>$movie['url'], 'video_id'=>$movie['mid']]).'?ps=s');
                    }
                } catch (Exception $ex) {
                    //ResultPrinter::printError("Get Payment", "Payment", null, null, $ex);
                    exit(1);
                }
            } catch (Exception $ex) {
                //ResultPrinter::printError("Executed Payment", "Payment", null, null, $ex);
                exit(1);
            }
        } else {
            Response::redirect(App::urlFor('home').'?ps=pc');
//            echo "Payment Cancelled";
//            //ResultPrinter::printResult("User Cancelled the Approval", null);
//            exit;
        }


    }// Approval Premium

    public function Subscription() {

    $apiContext = new ApiContext(new OAuthTokenCredential(self::$clientId, self::$secretId));
	$apiContext->setConfig(
	        array(
	            'mode' => self::$paymentMode,
	            'log.LogEnabled' => true,
	            'log.FileName' => 'App/Storage/paypal/PayPal.log',
	            'log.LogLevel' => 'FINE'
	        )
	);
        $returnUrl = self::$host.'/approval-subscription?success=true';
    	$cancelUrl = self::$host.'/approval-subscription?success=false';
	        
	// Create a new instance of Plan object
	$plan = new Plan();


	// # Basic Information
	// Fill up the basic information that is required for the plan
	$plan->setName('T-Shirt of the Month Club Plan')
	    ->setDescription('Template creation.')
	    ->setType('fixed');
	
	// # Payment definitions for this billing plan.
	$paymentDefinition = new PaymentDefinition();
	
	// The possible values for such setters are mentioned in the setter method documentation.
	// Just open the class file. e.g. lib/PayPal/Api/PaymentDefinition.php and look for setFrequency method.
	// You should be able to see the acceptable values in the comments.
	$paymentDefinition->setName('Regular Payments')
	    ->setType('REGULAR')
	    ->setFrequency('Month')
	    ->setFrequencyInterval("2")
	    ->setCycles("12")
	    ->setAmount(new Currency(array('value' => 5, 'currency' => 'USD')));
	
	// Charge Models
//	$chargeModel = new ChargeModel();
//	$chargeModel->setType('SHIPPING')
//	    ->setAmount(new Currency(array('value' => 10, 'currency' => 'USD')));
//
//	$paymentDefinition->setChargeModels(array($chargeModel));
	
	$merchantPreferences = new MerchantPreferences();
	
	// ReturnURL and CancelURL are not required and used when creating billing agreement with payment_method as "credit_card".
	// However, it is generally a good idea to set these values, in case you plan to create billing agreements which accepts "paypal" as payment_method.
	// This will keep your plan compatible with both the possible scenarios on how it is being used in agreement.
	$merchantPreferences->setReturnUrl($returnUrl)
	    ->setCancelUrl($cancelUrl)
	    ->setAutoBillAmount("yes")
	    ->setInitialFailAmountAction("CONTINUE")
	    ->setMaxFailAttempts("0")
	    ->setSetupFee(new Currency(array('value' => 1, 'currency' => 'USD')));
	
	
	$plan->setPaymentDefinitions(array($paymentDefinition));
	$plan->setMerchantPreferences($merchantPreferences);
	
	
	// ### Create Plan
	try {
	    $createdPlan= $plan->create($apiContext);
	    $patch = new Patch();

	    $value = new PayPalModel('{
		       "state":"ACTIVE"
		     }');
	
	    $patch->setOp('replace')
	        ->setPath('/')
	        ->setValue($value);
	    $patchRequest = new PatchRequest();
	    $patchRequest->addPatch($patch);
	
	    $createdPlan->update($patchRequest, $apiContext);
	    
	    
	    $agreement = new Agreement();


        $today = new \DateTime('+1day');
        $stringTime = $today->format('Y-m-d').'T'.$today->format('H:i:s').'Z';

//        die($stringTime);

		$agreement->setName('Ramro Movie Subscription Agreement')
		    ->setDescription('Basic Agreement For Subscription')
//		    ->setStartDate('2015-11-17T9:45:04Z');
		    ->setStartDate($stringTime);

		// Add Plan ID
		// Please note that the plan Id should be only set in this case.
		$plan = new Plan();
		$plan->setId($createdPlan->getId());
		$agreement->setPlan($plan);
		
		// Add Payer
		$payer = new Payer();
		$payer->setPaymentMethod('paypal');
		$agreement->setPayer($payer);
		
		// Add Shipping Address
		$shippingAddress = new ShippingAddress();
		$shippingAddress->setLine1('111 First Street')
		    ->setCity('Saratoga')
		    ->setState('CA')
		    ->setPostalCode('95070')
		    ->setCountryCode('US');
		$agreement->setShippingAddress($shippingAddress);
		
		// For Sample Purposes Only.
		//$request = clone $agreement;
		
		// ### Create Agreement
		try {
		    // Please note that as the agreement has not yet activated, we wont be receiving the ID just yet.
		    $agreement = $agreement->create($apiContext);
		
		    // ### Get redirect url
		    // The API response provides the url that you must redirect
		    // the buyer to. Retrieve the url from the $agreement->getApprovalLink()
		    // method
		    $approvalUrl = $agreement->getApprovalLink();
		    Response::redirect($approvalUrl);
		
		} catch (Exception $ex) {
		
		    exit(1);
		}
		//echo "<pre>";
		//print_r($agreement);
	    
	} catch (Exception $ex) {

	    exit(1);
	}
	
    }// Subscription
    
        public function ApprovalSubscription() {

        $apiContext = self::ConnectApi();
        
        if (isset($_GET['success']) && $_GET['success'] == 'true') {
	
	    $token = $_GET['token'];
	    $agreement = new Agreement();
	    try {
	        // ## Execute Agreement
	        // Execute the agreement by passing in the token
	        $agreement->execute($token, $apiContext);
	    } catch (Exception $ex) {
	        //ResultPrinter::printError("Executed an Agreement", "Agreement", $agreement->getId(), $_GET['token'], $ex);
	        exit(1);
	    }
	
	    //ResultPrinter::printResult("Executed an Agreement", "Agreement", $agreement->getId(), $_GET['token'], $agreement);
	
	    // ## Get Agreement
	    // Make a get call to retrieve the executed agreement details
	    try {
	        $agreement = Agreement::get($agreement->getId(), $apiContext);
	    } catch (Exception $ex) {
	        //ResultPrinter::printError("Get Agreement", "Agreement", null, null, $ex);
	        exit(1);
	    }
	    	echo "<pre>";
		print_r($agreement);
	    //ResultPrinter::printResult("Get Agreement", "Agreement", $agreement->getId(), null, $agreement);
	
	} else {
		
		echo "Approval Cancelled";
	    //ResultPrinter::printResult("User Cancelled the Approval", null);
	}

        
    }// Approval Subscription


  }// Class
  
}// Namespace