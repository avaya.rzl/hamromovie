<?php
namespace Notification\Controller {
    use App;
    use Auth\Model\Auth as AuthModel;
    use DB;
    use Input;
    use Notification\Models\Notification as NotificationModel;
    use Pagination;
    use Request;
    use Response;
    use Validate;
    use View;

    class Notification
    {

        // HOME
        public function Home()
        {
            AuthModel::IsLogged(array('admin_role' => 1));

            try {

                $limit = 12;
                $start = 0;

                $p = Input::get('p');
                isset($p) ? $page = preg_replace("/[^0-9]/", " ", trim($p)) : $page = 1;

                $start = ($page - 1) * $limit;

                $sql = "SELECT
                                *
                        FROM
                               kripa_notification_history
                       ";

                // DEFINE SEARCH QUERY
                if ($q = preg_replace('/[^A-Za-z0-9 ]/', ' ', Input::params('q'))) {
                    $sql .= " WHERE type LIKE ? ";
                }

                // COUNT RECORDS
                DB::query($sql, array('%' . $q . '%'), true);
                $data['totalRecords'] = $count = count(DB::fetch_assoc_all());

                // DEFAULT ASSIGN ORDER
                $sql .= " ORDER BY id DESC LIMIT $start, $limit";

                // FINAL QUERY FOR THE RESULT
                DB::query($sql, array('%' . $q . '%'), true);
                $data['results'] = DB::fetch_assoc_all();

                // PAGINATION RESULT AND LINK
                $pg = new Pagination();
                $data['pagination'] = $pg->create_links($limit, $page, App::urlfor('notification') . '?q=' . $q . '&p=', $count);

            } catch (ResourceNotFoundException $e) {

                $data['errMsg'] = $e->getMessage();
            }

            // ASSIGN PAGE TO FORM TO RESUBMIT
            $data['page'] = $page;

            View::display('@Notification/notification-grid.twig', $data);
        }

        // ADD
        public function Insert()
        {
            $data = array();
            AuthModel::IsLogged(array('admin_role' => 1));

            // GRAB ALL INPUTS
            $input = (object)Input::post();

            if (Request::isPost()) {

                $data['v'] = Validate::Instance(); //OBJECT VALIDATE

                Validate::Str($input->title, $displayName = 'Title', $req = TRUE, $min = 3);
                Validate::Str($input->type, $displayName = 'Type', $req = TRUE, $min = 4);

                if (Validate::IsFine()) {

                    if (!NotificationModel::CheckTitle($input->title)) {

                        try {

                            DB::transaction_start();

                            $dataUser = array(

                                'title' => $input->title,
                                'type' => $input->type,
                                'created_at' => CUR_TIME

                            );

                            if (NotificationModel::Add($dataUser)) {

                                NotificationModel::SendToGcm(NotificationModel::GetAllIds(), array('title' => $input->title, 'type' => $input->type));
                                $data['succMsg'] = 'Notification Successfully Sent';
                                $data['reset'] = true;

                            } else {

                                $data['errMsg'] = "Error in Adding Data try again later";
                            }

                            DB::transaction_complete();

                        } catch (ResourceNotFoundException $e) {

                            $data['errMsg'] = $e->getMessage();
                        }
                    } else {
                        $data['errMsg'] = 'Title Already Exists';
                    }
                }

            }

            View::display('@Notification/notification-add.twig', $data);
        }

        // DELETE
        public function Delete()
        {
            AuthModel::IsLogged(array('admin_role' => 1));

            if (Request::isPost()) {

                $ids = Input::post('toDelete');

                if (count(Input::post()) > 0) {

                    if (NotificationModel::Remove($ids)) {

                        Response::redirect(App::urlFor('notification'));
                    }

                } else {

                    Response::redirect(App::urlFor('notification'));
                }

            }
        }

        // REGISTER GCM AND APNS IDS TO SERVER
        public function RegisterId()
        {

            $data = array();

            // GRAB ALL INPUTS
            $input = (object)Input::post();// key=>type,reg_id

            if (Request::isPost()) {

                Validate::Instance(); //OBJECT VALIDATE

                Validate::Str($input->type, $displayName = 'Notification Type', $req = TRUE, $min = 3);// gcm or apns
                Validate::Str($input->reg_id, $displayName = 'Registration Id', $req = TRUE, $min = 3);

                if (Validate::IsFine()) {

                    try {

                        DB::transaction_start();

                        $rawData = array(

                            'type' => $input->type,
                            'reg_id' => $input->reg_id,
                            'created_at' => CUR_TIME

                        );

                        if (NotificationModel::SaveRegId($rawData)) {

                            $code = 200;
                            $data['succMsg'] = "Successfully Saved";
                            $data['reset'] = true;

                        } else {

                            $code = 200;
                            $data['errMsg'] = "Error in Adding Data try again later";
                        }

                        DB::transaction_complete();

                    } catch (ResourceNotFoundException $e) {

                        $code = 200;
                        $data['errMsg'] = $e->getMessage();
                    }

                } else {

                    $code = 200;
                    $data['error'] = TRUE;
                    $data['message'] = "Field Error";
                    $data['errList'] = array(

                        'type' => Validate::getError('Notification Type'),
                        'reg_id' => Validate::getError('Registration Id')
                    );

                }

            }
            Response::json($code, $data);
        }

        // SEND PUSH NOTIFICATION TO DEVICE FOR TESTING
        public function TestNotification()
        {

            $data = array();

            if (Request::isPost()) {

                $regIds = Input::post('reg_id');
                $title = Input::post('title');
                $type = Input::post('type');

                Validate::Str($title, 'Title', TRUE, 4);
                Validate::Str($type, 'Type', TRUE, 2, 19);
                Validate::Str($regIds, 'Reg Id', TRUE, 2);

                if (Validate::isFine()) {

                    $msgNotification = array(
                        "title" => $title,
                        "type" => $type
                    );

                    //echo GOOGLE_GCM_API_KEY;exit;
                    $response = NotificationModel::SendToGcm(array($regIds), $msgNotification);

                    if ($response !== FALSE) {

                        $code = 200;
                        $data['error'] = FALSE;
                        $data['message'] = "Notification Successfully Sent ";
                        $data['format'] = $msgNotification;
                        $data['gcm_result'] = $response;

                    } else {

                        $code = 200;
                        $data['error'] = TRUE;
                        $data['message'] = "Failed to Send Notification ";
                        $data['format'] = $msgNotification;
                        $data['gcm_result'] = $response;
                    }
                } else {

                    $code = 200;
                    $data['error'] = TRUE;
                    $data['message'] = "Field Error";
                    $data['errList'] = array(

                        'title' => Validate::getError('Title'),
                        'type' => Validate::getError('Type'),
                        'reg_id' => Validate::getError('Reg Id')

                    );
                }
                Response::json($code, $data);

            }
        }

    }
}