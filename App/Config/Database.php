<?php
/*
 |=====================================================================================================
 | Database Connection
 |=====================================================================================================
 | Zebra Database Eloquent or redbean
 |
 |
 */

$config = array(

    'database' => array(

        'driver'    => 'mysql',
        'host'      => DB_HOST,
        'database'  => DB_NAME,
        'username'  => DB_USER,
        'password'  => DB_PASS,
        'charset'   =>  'utf8',
        'collation' => 'utf8_general_ci',
        'prefix'    => ''
    )
);

$db = new Zebra_Database();

//memcache
$db->memcache_host          = 'localhost';
$db->memcache_port          = 11211;
$db->memcache_compressed    = true;
$db->caching_method         = 'memcached';
$db->debug                  = APP_MODE === 'development'? true :false ;

$db->connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['database']);

$app->container->singleton('DB', function () use ($db) {
      return  $db;
});




// Eloquent Database
//use Illuminate\Cache\CacheManager as CacheManager;
//use Illuminate\Cache\MemcachedConnector as MemcachedConnector;
//use Illuminate\Database\Capsule\Manager as Capsule;
//use Illuminate\Filesystem\Filesystem as Filesystem;
//
//$capsule = new Capsule;
//$capsule->setFetchMode(PDO::FETCH_OBJ);
//$capsule->addConnection($config['database']);
//$container = $capsule->getContainer();
//
//$container['config']['cache.driver'] = 'file';// file | memcached
//$container['config']['cache.path'] = APP_FOLDER . '/Storage/cache/eloquent';
//$container['files'] = new Filesystem();
//$container['config']['cache.connection'] = null;
//$container['config']['cache.memcached'] = array(
//    array('host' => '127.0.0.1', 'port' => 11211, 'weight' => 100),
//);
//$container['config']['cache.prefix'] = 'RNSlim';
//$container['config']['cache.connector'] = new MemcachedConnector;
//
//$cacheManager = new CacheManager($container);
//$capsule->setCacheManager($cacheManager);
//$capsule->bootEloquent();
//
//// ADD SINGLETON TO APP
//$app->container->singleton('DB', function () use ($capsule, $config) {
//    $capsule->addConnection($config['database']);
//    return $capsule->getConnection();
//});


/*
|=====================================================================================================
| Redbean Database Configuration
|=====================================================================================================
| Redbean Database Configuration
|
|
*/
//R::setup('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
//R::freeze(false);// Disable Fluid Model

// Location:: Application/Config/Database.php
