<?php
/*
|=====================================================================================================
| Modules configuration
|=====================================================================================================
| Configure Your module
|
|
*/

$config['modules'] = array(
    'Admin',
    'Site',
    'Api',
    'Auth',
    'Notification',
    'change_to_your_module_name'
);

$twig   = App::View()->getInstance();
$loader = $twig->getLoader();

foreach ($config['modules'] as $mod) {
    $_viewPath = MODULE_PATH . $mod . '/Views/';
    if (is_dir($_viewPath)) {
        $loader->setPaths($_viewPath, $mod);
    }
}
$loader->addPath(APP_FOLDER . '/Storage/Error/Views','error');


// AUTO LOADS ALL .php FILE FROM DIRECTORIES (Deep Search Method)
foreach ($config['modules'] as $module) {
    $moduleDirs[] = MODULE_PATH . $module;
}

DirScan::fileList($moduleDirs);

// Location:: Application/Config/Modules.php