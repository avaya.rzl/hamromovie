<?php

/*
|=====================================================================================================
| Application access constants
|=====================================================================================================
| Define Your Constants Here And Use All Over Your Projects Freely
|
|
*/


// MODE OF PROJECT
DEFINE('APP_MODE', 'development'); // development | production

// WEBSITE NAME
DEFINE('WEBSITE_NAME', 'http://localhost/hamromovie/'); // Change it to your own host email

// HOST EMAIL
DEFINE('HOST_EMAIL', 'danepliz@gmail.com'); // Change it to your own host email

// SECRET KEY
DEFINE('SECRET_KEY', '1qR76jifwXsdfsdfB8tFi2a06RufeJwj1f6A2e'); //Use Secret Key For Encryption

// DEFAULT TIMEZONE
DEFINE('DEFAULT_TIME_ZONE', 'UTC');

// APPLICATION FOLDER
DEFINE('APP_FOLDER',ROOT . 'App');

// MODULE FOLDER PATH
DEFINE('MODULE_PATH', APP_FOLDER.'/Modules/');

// DEFAULT PAGE LIMIT
DEFINE('PAGE_LIMIT',6);


/*
|=====================================================================================================
| Database access constants
|=====================================================================================================
| Database Configuration Constants
|
|
*/

// CONN1
// DATABASE HOST NAME
DEFINE('DB_HOST', 'localhost');

// DATABASE NAME
DEFINE('DB_NAME', 'db_hamro_movie');

// DATABASE USERNAME
DEFINE('DB_USER','root');

// DATABASE PASSWORD
DEFINE('DB_PASS', 'r00t');

// Enable Cache in file of Memcached
DEFINE('DB_CACHE', 'memcached');//memcached or file
DEFINE('QUERY_CACHE',true);

// CURRENT TIME FORMAT
DEFINE('CUR_TIME',date('Y-m-d H:i:s'));

// REDBEAN FREEZE MODE
DEFINE('FREEZE',TRUE);

// BASE URL
DEFINE('BASE_URL', 'http://localhost/hamromovie');



/** Email Configuration */
//DEFINE('MAILER_HOST', 'https://p3plcpnl0787.prod.phx3.secureserver.net');
//DEFINE('MAILER_PORT', 993); //995
//DEFINE('MAILER_USERNAME', 'info@bidhee.net');
//DEFINE('MAILER_PASSWORD', 'Bidhee54321');
//DEFINE('FEEDBACK_RECEIVER_EMAIL', 'danepliz@gmail.com');
//DEFINE('FEEDBACK_RECEIVER_NAME', 'Hamro Movie Feedback');


DEFINE('MAILER_HOST', 'ssl://smtp.gmail.com');
DEFINE('MAILER_PORT', 465); //995 993 465
DEFINE('MAILER_USERNAME', 'info.bidhee@gmail.com');
DEFINE('MAILER_PASSWORD', '9851120083'); //9851120083
DEFINE('FEEDBACK_RECEIVER_EMAIL', 'danepliz@gmail.com');
DEFINE('FEEDBACK_RECEIVER_NAME', 'Hamro Movie Feedback');


DEFINE('GOOGLE_CLIENT_ID','608403493595-rp3ums7rg2ltb4cslhpah569c52jjgup.apps.googleusercontent.com');
DEFINE('GOOGLE_CLIENT_SECRET','scO-0IPGpa4e-lqcZEzs3swb');
DEFINE('GOOGLE_REDIRECT_URL', BASE_URL.'/gplus-login');
DEFINE('GOOGLE_DEVELOPERS_KEY', 'AIzaSyDuks_FjUFt_9bDWS5fsHlYh6bEkS_O4-4');

DEFINE('FB_APP_ID', '1043701965649365');
DEFINE('FB_APP_SECRET', '66e6e31bdaaa2c6796c65fbf6ef4fcd3');


DEFINE('PAYPAL_CLIENT_ID', 'AQo7lqgl7sJAvUPJwisBRCJVbYHzAaQFV2kl0h-ckOoM6216WYnl8Y_oQBhioPgDwQCF4NhF8a-QRwa5');
DEFINE('PAYPAL_SECRETE_ID', 'ELdNugNfTxOQHRQVqMXrTtO__INddF6PAp9owKJcRBnmhJSS19GF72ePrXrss8pcclLlurLnK9aBbpN5');
DEFINE('PAYPAL_PAYMENT_MODE', 'sandbox');


//DEFINE('PAYPAL_CLIENT_ID', 'AR_vuYfdlP_D3iFhF8SdLGFk2zAElefj-F9WPmPmQjUZjiApsr587enpSjC8Mae0wXNjThktG8MCCIyk');
//DEFINE('PAYPAL_SECRETE_ID', 'EPsqZ3ODJT85IdMnd9HV4rLIBgS3EBD7s1eoeBJ_fNL-vdd3tjTMWDORm-ZI05MlwIkUpx2lureNh2uA');
//DEFINE('PAYPAL_PAYMENT_MODE', 'live');

//<script>
//window.fbAsyncInit = function() {
//    FB.init({
//      appId      : '1043701965649365',
//      xfbml      : true,
//      version    : 'v2.5'
//    });
//  };
//
//(function(d, s, id){
//    var js, fjs = d.getElementsByTagName(s)[0];
//     if (d.getElementById(id)) {return;}
//     js = d.createElement(s); js.id = id;
//     js.src = "//connect.facebook.net/en_US/sdk.js";
//     fjs.parentNode.insertBefore(js, fjs);
//   }(document, 'script', 'facebook-jssdk'));
//</script>

//