<?php

/*
|=====================================================================================================
| Application access constants
|=====================================================================================================
| Define Your Constants Here And Use All Over Your Projects Freely
|
|
*/


// MODE OF PROJECT
DEFINE('APP_MODE', 'production'); // development | production

// WEBSITE NAME
DEFINE('WEBSITE_NAME', 'http://hamromovie.com/'); // Change it to your own host email

// HOST EMAIL
DEFINE('HOST_EMAIL', 'info@bidhee.net'); // Change it to your own host email

// SECRET KEY
DEFINE('SECRET_KEY', '1qR76jifwXB8tFi2a06RufeJwj1f6A2e'); //Use Secret Key For Encryption

// DEFAULT TIMEZONE
DEFINE('DEFAULT_TIME_ZONE', 'UTC');

// APPLICATION FOLDER
DEFINE('APP_FOLDER',ROOT . 'App');

// MODULE FOLDER PATH
DEFINE('MODULE_PATH', APP_FOLDER.'/Modules/');

// DEFAULT PAGE LIMIT
DEFINE('PAGE_LIMIT',6);


/*
|=====================================================================================================
| Database access constants
|=====================================================================================================
| Database Configuration Constants
|
|
*/

// CONN1
// DATABASE HOST NAME
DEFINE('DB_HOST', 'localhost');

// DATABASE NAME
DEFINE('DB_NAME', 'ramro_movie');

// DATABASE USERNAME
DEFINE('DB_USER','root');

// DATABASE PASSWORD
DEFINE('DB_PASS', '3c2@m0vie');

// Enable Cache in file of Memcached
DEFINE('DB_CACHE', 'memcached');//memcached or file
DEFINE('QUERY_CACHE',true);

// CURRENT TIME FORMAT
DEFINE('CUR_TIME',date('Y-m-d H:i:s'));

// REDBEAN FREEZE MODE
DEFINE('FREEZE',TRUE);

// BASE URL
DEFINE('BASE_URL', 'http://hamromovie.com');
//DEFINE('BASE_URL', 'http://52.25.42.4/hamromovie');



/** Email Configuration */
//DEFINE('MAILER_HOST', 'https://p3plcpnl0787.prod.phx3.secureserver.net');
//DEFINE('MAILER_PORT', 993); //995
//DEFINE('MAILER_USERNAME', 'info@bidhee.net');
//DEFINE('MAILER_PASSWORD', 'Bidhee54321');
//DEFINE('FEEDBACK_RECEIVER_EMAIL', 'danepliz@gmail.com');
//DEFINE('FEEDBACK_RECEIVER_NAME', 'Hamro Movie Feedback');


DEFINE('MAILER_HOST', 'ssl://smtp.gmail.com');
DEFINE('MAILER_PORT', 465); //995 993 465
DEFINE('MAILER_USERNAME', 'info.bidhee@gmail.com');
DEFINE('MAILER_PASSWORD', '9851120083'); //9851120083
DEFINE('FEEDBACK_RECEIVER_EMAIL', 'danepliz@gmail.com');
DEFINE('FEEDBACK_RECEIVER_NAME', 'Hamro Movie Feedback');
